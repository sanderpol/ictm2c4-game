package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.mygdx.game.screens.mainMenu;


public class PacmanGame extends Game {
	@Override
	public void create() {

		setScreen(new mainMenu(this));
		// create the ArduinoComm instance
		ArduinoComm.instance = new ArduinoComm();

	}

	@Override
	public void dispose() {
		ArduinoComm.closeArduinoPort();

	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}
}
