package com.mygdx.game;

import com.fazecast.jSerialComm.SerialPort;

import javax.swing.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ArduinoComm {
    public static ArduinoComm instance;

    private SerialPort port = null;
    private JFrame parentFrame;
    private String message;

    private long arduioReceiveTimer, lastArduinoReceiveTime;

    public ArduinoComm(JFrame parentFrame) {
        this.parentFrame = parentFrame;
        ArduinoComm.instance = this;
        openPort();
    }

    public ArduinoComm() {
        // if no parent frame is given, create one
        this(new JFrame());
    }

    private void openPort() {
        // loop through all ports
        SerialPort[] allPorts = SerialPort.getCommPorts();

        // if no ports are found, show error and return without opening one
        if (allPorts.length == 0) {
            showError(parentFrame, "Could not find any port. \n" +
                    "Please check if the arduino had been connected");
            return;
        }
        for (SerialPort p : allPorts) {
            port = SerialPort.getCommPort(p.getSystemPortName());

            // attempt to open the port
            port.openPort(1000);
            if (!port.isOpen()) {
                showError(parentFrame, "Port not available");
            }
        }
        // default connection settings for Arduino
        port.setComPortParameters(9600, 8, 1, 0);
        // Timeout setting to make sure writing and reading is done correctly
        port.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING | SerialPort.TIMEOUT_WRITE_BLOCKING,
                0, 0);
    }

    public void sendByte(byte b) {
        if (port == null) return;
        try {
            port.getOutputStream().write(b);
            port.getOutputStream().flush();
        } catch (IOException e) {
            showError(parentFrame, "Error while writing to the Arduino: " + e.getCause());
        }
    }

    public void sendBytes(byte... bytes) {
        if (port == null) return;
        try {
            for (byte b : bytes) {
                port.getOutputStream().write(b);
            }
            port.getOutputStream().flush();
        } catch (IOException e) {
            showError(parentFrame, "Error while writing to the Arduino: " + e.getCause());
        }
    }

    public void send(String message) {
        if (port == null) return;
        // attempt to write a string to the Arduino
        try {
            port.getOutputStream().write(message.getBytes());
            port.getOutputStream().flush();
        } catch (IOException e) {
            showError(parentFrame, "Error while writing to the Arduino: " + e.getCause());
        }
    }

    public String[] receive() {
        if (port == null) return new String[]{"No open port"};

        // if more then .1 seconds have passed after the last call to the Arduino, retrieve the new messgae
        arduioReceiveTimer += System.currentTimeMillis() - lastArduinoReceiveTime;
        lastArduinoReceiveTime = System.currentTimeMillis();
        if (arduioReceiveTimer >= 100) {
            arduioReceiveTimer = 0;

            // tell the Arduino to send the next message
            sendByte((byte) 1);
            message = "";
            // wait at most 1 minute for data to come in
            long currentTime = System.currentTimeMillis();
            long stopTime = currentTime + 60000;
            while (System.currentTimeMillis() < stopTime) {
                try {
                    // read all the incomming bytes and put them into a string
                    byte[] b = port.getInputStream().readNBytes(port.bytesAvailable());
                    message = new String(b, StandardCharsets.UTF_8);
                    // if the message contains data, break the loop
                    if (!message.equals("")) {
                        break;
                    }
                } catch (IOException e) {
                    showError(parentFrame, "Error while reading from the Arduino");
                }
            }

            if (!message.equals("")) {
                // return the message, split on the comma's
                return message.split(",");
            } else {
                showError(parentFrame, "The Arduino took to long to respond. \n" +
                        "Please restart the application.");
                return new String[]{"Arduino did not respond"};
            }
        }

        // return the last message, split on the comma's
        return message.split(",");

    }

    private static void showError(JFrame parent, String errorMessage) {
        JOptionPane.showMessageDialog(parent, errorMessage, "An error occured", JOptionPane.ERROR_MESSAGE);
    }

    public static void closeArduinoPort(){
        instance.port.closePort();
    }
}