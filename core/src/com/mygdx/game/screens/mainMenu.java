package com.mygdx.game.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.mygdx.game.PlayMap;


public class mainMenu implements Screen {

    private Stage stage;
    private TextButton button;
    private TextButton.TextButtonStyle textButtonStyle;
    private BitmapFont font;
    private Skin skin;
    private TextureAtlas buttonAtlas;

    private Game game;

    public mainMenu(Game game) {
        this.game = game;
    }

    @Override
    public void show() {

        Texture logo = new Texture("entities/pacWoman.png");

        Table table = new Table();

        Image logoImage = new Image(logo);
        table.add(logoImage);


        stage = new Stage();

        Gdx.input.setInputProcessor(stage);

        font = new BitmapFont();
        skin = new Skin();

        buttonAtlas = new TextureAtlas("buttons/StartButtons.pack");

        skin.addRegions(buttonAtlas);

        textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = font;
        textButtonStyle.up = skin.getDrawable("saveGameButton");
        textButtonStyle.down = skin.getDrawable("saveGameButtonDown");


        button = new TextButton("", textButtonStyle);
        button.setPosition(Gdx.graphics.getWidth() / 2 - button.getWidth() / 2, Gdx.graphics.getHeight() / 2 - 30);

        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new PlayMap());
            }
        });

        stage.addActor(button);
        stage.addActor(table);


    }

    @Override
    public void render(float delta) {
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
