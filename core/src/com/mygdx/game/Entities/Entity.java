package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.PlayMap;


import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public abstract class Entity extends Sprite implements InputProcessor {

    /**
     * movement speed
     */
    protected Vector2 vector2 = new Vector2();
    protected final float SPEED = 90;
    protected float increment;
    private int distanceWall = 4;
    private TiledMapTileLayer collisionLayer;

    private boolean[] directionsBlocked;
    private Queue<Integer> directionsLast5;
    private String blockedKey = "blocked";
    private int currentDirection;
    private int queueCapacity = 10;
    private int tileWidth, tileHeight;

    private int score = 0;

    public Entity(Sprite sprite, TiledMapTileLayer collisionLayer) {
        super(sprite);
        this.collisionLayer = collisionLayer;
        this.directionsBlocked = new boolean[4];
        this.directionsLast5 = new LinkedBlockingQueue<>(queueCapacity);
        for (int i = 0; i < queueCapacity; i++) {
            this.directionsLast5.add(2);
        }
        this.tileWidth = (int) collisionLayer.getTileWidth();
        this.tileHeight = (int) collisionLayer.getTileHeight();


    }


    @Override
    public void draw(Batch batch) {
        update(Gdx.graphics.getDeltaTime());
        super.draw(batch);
    }

    public void update(float delta) {

        int lastDirection = directionsLast5.poll();

        //Set the old X and Y values before proceeding
        float oldX = getX(), oldY = getY();
        boolean collidedX = false, collidedY = false;


        checkAdjacentTiles(getX(), getY());


        //Set the Entity to the new X Value
        setX(getX() + vector2.x * delta);

        FoodEntity entity = PlayMap.foodEntityList.getEntity((int) getX() / 32, (int) getY() / 32);

        if(entity != null)
        {
            if(entity instanceof PowerUpEntity)
            {
                PlayMap.foodEntityList.removeEntity((int) oldX / 32, (int) oldY / 32);
                score += 200;
                Gdx.graphics.setTitle("Pacman | Score : " + score);
            } else {
                PlayMap.foodEntityList.removeEntity((int) oldX / 32, (int) oldY / 32);
                score += 100;
                Gdx.graphics.setTitle("Pacman | Score : " + score);
            }

        }


        //increment calculation for the x- as
        increment = collisionLayer.getTileWidth() / 2;
        increment = getWidth() < increment ? getWidth() / 2 : increment / 2;

        if (vector2.x < 0) {
            collidedX = collideLeft();
            currentDirection = 3;
        } else if (vector2.x > 0) {
            collidedX = collideRight();
            currentDirection = 1;
        }

        //on y-axis movement
        setY(getY() + vector2.y * delta);

        //increment calculation for the y-as
        increment = collisionLayer.getTileHeight() / 2;
        increment = getHeight() < increment ? getHeight() / 2 : increment / 2;

        if (vector2.y < 0) {
            collidedY = collideBottom();
            currentDirection = 2;

        } else if (vector2.y > 0) {
            collidedY = collideTop();
            currentDirection = 0;
        }

        for (boolean b : directionsBlocked
        ) {
            System.out.print(b + ",");

        }
        System.out.println(currentDirection);



        if (collidedX) {
            setX(oldX);

            if (!directionsBlocked[lastDirection]) {
                setNewDirection(lastDirection);
            }

//            System.out.println("Hit wallX");
        }

        if (collidedY) {
            setY(oldY);
            if (!directionsBlocked[lastDirection]) {
                setNewDirection(lastDirection);
            }


//            System.out.println("Hit wallY");

        }
        directionsLast5.add(currentDirection);

    }

    protected void setNewDirection(int newDirection) {
        if (newDirection == 0) {
            vector2.x = 0;
            vector2.y = +SPEED;
        } else if (newDirection == 1) {
            vector2.y = 0;
            vector2.x = +SPEED;
        } else if (newDirection == 2) {
            vector2.x = 0;
            vector2.y = -SPEED;
        } else if (newDirection == 3) {
            vector2.y = 0;
            vector2.x = -SPEED;
        }
        if (this instanceof PacmanEntity){
            ((PacmanEntity) this).setDirection(newDirection);
        }
    }


    //Check if adjacentTiles are available and reachable
    private void checkAdjacentTiles(float x, float y) {


        //Check if top cell if blocked
        if (isCellBlocked(x + tileWidth / 2, y + tileHeight / 2 + tileHeight)) {
            directionsBlocked[0] = true;
        } else {
            directionsBlocked[0] = false;
        }

        //Check if right cell if blocked
        if (isCellBlocked(x + tileWidth / 2 + tileWidth, y + tileHeight / 2)) {
            directionsBlocked[1] = true;
        } else {
            directionsBlocked[1] = false;
        }

        //Check if bottom cell if blocked
        if (isCellBlocked(x + tileWidth / 2, y + tileHeight / 2 - tileHeight)) {
            directionsBlocked[2] = true;
        } else {
            directionsBlocked[2] = false;
        }

        //Check if left cell if blocked
        if (isCellBlocked(x + tileWidth / 2 - tileWidth, y + tileHeight / 2)) {
            directionsBlocked[3] = true;
        } else {
            directionsBlocked[3] = false;
        }


    }

    private boolean collideBottom() {
        for (float i = 0; i <= getHeight(); i += increment) {
            if (isCellBlocked(getX() + i, getY())) {
                return true;
            }
        }
        return false;
    }

    private boolean collideTop() {
        for (float i = 0; i <= getHeight(); i += increment) {
            if (isCellBlocked(getX() + i, getY() + getHeight())) {
                return true;
            }
        }
        return false;
    }

    private boolean collideRight() {
        for (float i = 0; i <= getHeight(); i += increment) {
            if (isCellBlocked(getX() + getWidth(), getY() + i)) {
                return true;
            }
        }
        return false;
    }

    private boolean collideLeft() {
        for (float i = 0; i <= getHeight(); i += increment) {
            if (isCellBlocked(getX(), getY() + i)) {
                return true;
            }
        }
        return false;
    }

    public TiledMapTileLayer getCollisionLayer() {
        return collisionLayer;
    }


    public boolean isCellBlocked(float x, float y) {

        //Create cell object
        TiledMapTileLayer.Cell cell = collisionLayer.getCell((int) (x / collisionLayer.getTileWidth()), (int) (y / collisionLayer.getTileHeight()));

        //If the cell is blocked, return true
        if(cell != null
                && cell.getTile() != null
                && cell.getTile().getProperties().containsKey(blockedKey))
        {
            return true;
        }

        //If the cell is a ghostDoor cell, also return true
        if(cell != null
                && cell.getTile() != null
                && cell.getTile().getProperties().containsKey("ghostDoor"))
        {
            return true;
        }

        //If none of the above applies, return false: cell is not blocked
        return false;



        //return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey(blockedKey);
    }

    public boolean isCellSpawn(int x, int y)
    {
        TiledMapTileLayer.Cell cell = collisionLayer.getCell(x, y);

        if(cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey("spawn"))
        {
            return true;
        } else {
            return false;
        }
    }


}
