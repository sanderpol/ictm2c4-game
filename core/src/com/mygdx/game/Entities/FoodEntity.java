package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

public class FoodEntity extends Sprite {

    protected TiledMapTileLayer collisionLayer;

    private final int DISTANCEWALL = 4;

    public int x;
    public int y;




    public FoodEntity(Sprite sprite, TiledMapTileLayer collisionLayer, int x, int y)
    {
        super(sprite);
        this.collisionLayer = collisionLayer;
    }

    @Override
    public void draw(Batch batch) {
        update(Gdx.graphics.getDeltaTime());
        super.draw(batch);
    }

    private void update(float delta) {


    }

    public TiledMapTileLayer getCollisionLayer()
    {
        return this.collisionLayer;
    }


}
