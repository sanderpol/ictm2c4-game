package com.mygdx.game.Entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

public class PowerUpEntity extends FoodEntity {


    public PowerUpEntity(TiledMapTileLayer collisionLayer, int x, int y) {
        super(new Sprite(new Texture("entities/bigDot.png")), collisionLayer, x, y);
    }
}
