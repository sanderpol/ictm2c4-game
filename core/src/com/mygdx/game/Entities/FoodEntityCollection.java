package com.mygdx.game.Entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;


public class FoodEntityCollection {

    private FoodEntity[][] entities;

    public FoodEntityCollection(){

        entities = new FoodEntity[27][];

        for(int i = 0; i < 27; i++)
        {
            entities[i] = new FoodEntity[31];
        }
    }

    public void addFoodEntity(FoodEntity e)
    {
        entities[e.x][e.y] = e;
    }

    public void addPowerUpEntity(PowerUpEntity e)
    {
        entities[e.x][e.y] = e;
    }

    public void addFoodEntity(TiledMapTileLayer collisionLayer, int x, int y)
    {
        //Create food entity
        FoodEntity e = new FoodEntity(new Sprite(new Texture("entities/dot.png")), collisionLayer, x, y);

        //Insert entity into the array
        entities[x][y] = e;
    }

    public void addPowerUpEntity(TiledMapTileLayer collisionLayer, int x, int y)
    {
        //Create food entity

        PowerUpEntity e = new PowerUpEntity(collisionLayer, x, y);

        //Insert entity into the array
        entities[x][y] = e;
    }

    public boolean hasFood(int x, int y)
    {
        if(entities[x][y] instanceof FoodEntity || entities[x][y] instanceof PowerUpEntity)
        {
            return true;
        } else {
            return false;
        }
    }

    public FoodEntity getEntity(int x, int y)
    {
        if(entities[x][y] != null)
        {
            return entities[x][y];
        }else{return null;}
    }

    public void removeEntity(int x, int y)
    {
        entities[x][y] = null;
    }
}
