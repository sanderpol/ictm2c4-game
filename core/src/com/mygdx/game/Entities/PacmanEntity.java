package com.mygdx.game.Entities;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.mygdx.game.ArduinoComm;
import com.mygdx.game.animations.Animation;


public class PacmanEntity extends Entity implements InputProcessor {
    private ArduinoComm arduino;
    private int joystickX;
    private int joystickY;
    private int direction;
    private Animation pacmanAnimationUp;
    private Animation pacmanAnimationDown;
    private Animation pacmanAnimationLeft;
    private Animation pacmanAnimationRight;
    private Texture pacmanUp, pacmanDown, pacmanLeft, pacmanRight;

    public PacmanEntity(Sprite sprite, TiledMapTileLayer collisionLayer) {
        super(sprite, collisionLayer);
        arduino = ArduinoComm.instance;
        this.setPosition(13 * this.getCollisionLayer().getTileWidth(),
                (this.getCollisionLayer().getHeight() - 20) * this.getCollisionLayer().getTileHeight());
        this.pacmanUp = new Texture("entities/pmU.png");
        pacmanAnimationUp = new Animation(pacmanUp, 3, 0.5f);
        this.pacmanDown = new Texture("entities/pmD.png");
        pacmanAnimationDown = new Animation(pacmanDown, 3, 0.5f);
        this.pacmanLeft = new Texture("entities/pmL.png");
        pacmanAnimationLeft = new Animation(pacmanLeft, 3, 0.5f);
        this.pacmanRight = new Texture("entities/pmR.png");
        pacmanAnimationRight = new Animation(pacmanRight, 3, 0.5f);
        this.direction = 2;
    }


    @Override
    public void update(float delta) {


//         get the latest message from the Arduino
        String[] arduinoMessage = arduino.receive();

        // get the values for the joystick
        // if the Arduino sends an unusable string, set the joystick to its center position
        try {
            joystickX = Integer.parseInt(arduinoMessage[0]);
            joystickY = Integer.parseInt(arduinoMessage[1]);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            joystickX = 512;
            joystickY = 512;
        }

        // check if the joystick is not in its center position
        if (!(joystickY > 412 && joystickY < 612 && joystickX > 412 && joystickX < 612)) {
            if (joystickY < 512 && ((joystickX < 512 && joystickY < joystickX) ||
                    (joystickX > 512 && (1024 - joystickY) > joystickX))) {
                direction = 0;
            } else if ((joystickY < 512 && (joystickX < 512 && joystickY > joystickX)) ||
                    (joystickY > 512 && (joystickX < 512 && (1024 - joystickY) > joystickX))) {
                direction = 3;
            } else if ((joystickY < 512 && (joystickX > 512 && (1024 - joystickY) < joystickX)) ||
                    (joystickY > 512 && (joystickX > 512 && joystickY < joystickX))) {
                direction = 1;
            } else if (joystickY > 512 && ((joystickX < 512 && (1024 - joystickY) < joystickX) ||
                    (joystickX > 512 && joystickY > joystickX))) {
                direction = 2;
            }
            super.setNewDirection(direction);
        }

        switch (direction) {
            case 0:
                pacmanAnimationUp.update(delta);
                break;
            case 1:
                pacmanAnimationRight.update(delta);
                break;
            case 2:
                pacmanAnimationDown.update(delta);
                break;
            case 3:
                pacmanAnimationLeft.update(delta);
                break;
        }

        super.update(delta);
    }


    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Keys.W:
                direction = 0;
                super.setNewDirection(0);
                break;
            case Keys.A:
                direction = 3;
                super.setNewDirection(3);
                break;
            case Keys.S:
                direction = 2;
                super.setNewDirection(2);
                break;
            case Keys.D:
                direction = 1;
                super.setNewDirection(1);
                break;
        }


        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public TextureRegion getTextureFrame() {
        switch (direction) {
            case 0:
                return pacmanAnimationUp.getFrame();
            case 1:
                return pacmanAnimationRight.getFrame();
            case 2:
                return pacmanAnimationDown.getFrame();
            case 3:
                return pacmanAnimationLeft.getFrame();
        }
        return pacmanAnimationDown.getFrame();
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
}
