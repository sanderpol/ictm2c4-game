package com.mygdx.game.animations;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

public class Animation {
    private Array<TextureRegion> frames;
    private float maxFrameTime;
    private float currentFrametime;
    private int framecount;
    private int frame;

    public Animation(Texture texture, int framecount, float cycleTime) {
        frames = new Array<TextureRegion>();
        int frameWidth = texture.getWidth() / framecount;
        for (int i = 0; i < framecount; i++) {
            frames.add(new TextureRegion(texture, i * frameWidth, 0, frameWidth, texture.getHeight()));

        }
        this.framecount = framecount;
        maxFrameTime = cycleTime / framecount;
        frame = 0;
    }

    public void  update(float delta){
        currentFrametime += delta;
        if (currentFrametime > maxFrameTime){
            frame++;
            currentFrametime = 0;
        }
        if (frame >= framecount){
            frame = 0;
        }
    }

    public TextureRegion getFrame(){
        return frames.get(frame);
    }
}
