package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.mygdx.game.Entities.FoodEntityCollection;
import com.mygdx.game.Entities.PacmanEntity;

import java.util.ArrayList;

public class PlayMap implements Screen {

    private TiledMap tiledMap;
    private OrthogonalTiledMapRenderer renderer;
    private OrthographicCamera camera;
    private PacmanEntity pacman;

    public static FoodEntityCollection foodEntityList;

    private float height;
    private float width;


    @Override
    public void show() {

        foodEntityList = new FoodEntityCollection();

        tiledMap = new TmxMapLoader().load("maps/final-map.tmx");

        renderer = new OrthogonalTiledMapRenderer(tiledMap);

        camera = new OrthographicCamera();

        pacman = new PacmanEntity(new Sprite(new Texture("entities/PacmanSmall.png")), (TiledMapTileLayer) tiledMap.getLayers().get(0));

        height = pacman.getCollisionLayer().getTileHeight();
        width = pacman.getCollisionLayer().getTileHeight();
        TiledMapTileLayer collisionLayer = pacman.getCollisionLayer();


        TiledMapTileLayer powerupLayer = (TiledMapTileLayer) tiledMap.getLayers().get("powerups");



        //Loop over the y axis
        for(int y = 0; y < 31; y++)
        {

            //For every y row, loop over all the tiles in the x-direction
            for(int x = 0; x < 27; x++)
            {

                //Check if pill should be placed there (only on non-blocked tiles)
                if(!pacman.isCellBlocked(x * width, y * height))
                {
                    //Get the cell from the powerup layer
                    TiledMapTileLayer.Cell cell = powerupLayer.getCell(x, y);

                    //Skip food entity if tile is a spawn tile
                    if(cell !=  null && cell.getTile().getProperties().containsKey("spawn"))
                    {
                        continue;
                    }

                    //Check if tile is a powerup tile
                    if(cell != null && cell.getTile().getProperties().containsKey("powerup"))
                    {
                        System.out.println(cell.getTile().getObjects().getCount() == 1);
                        foodEntityList.addPowerUpEntity(collisionLayer, x, y);
                        System.out.println("X: " + x + "Y: " + y);

                    //Tile is regular tile
                    } else {
                        foodEntityList.addFoodEntity(collisionLayer, x, y);
                    }

                }

            }
        }

        Gdx.input.setInputProcessor(pacman);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor( 0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        renderer.setView(camera);
        renderer.render();

        renderer.getBatch().begin();
        pacman.setRegion(pacman.getTextureFrame());

        //Render the pacman
        pacman.draw(renderer.getBatch());


        for( int y = 0; y < 31; y++)
        {
            for(int x = 0; x < 27; x++)
            {
                //Draw the food entity if not null
                if(foodEntityList.getEntity(x, y) != null)
                {
                    foodEntityList.getEntity(x, y).draw(renderer.getBatch());
                    foodEntityList.getEntity(x, y).setPosition(x * width, y * height);
                }
            }
        }

        renderer.getBatch().end();


    }

    @Override
    public void resize(int width, int height) {
        camera.viewportWidth = width;
        camera.viewportHeight = height;
        camera.position.x = width/2f;
        camera.position.y = height/2f;
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        tiledMap.dispose();
        renderer.dispose();
        pacman.getTexture().dispose();
    }
}
