<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.4" name="tiles" tilewidth="32" tileheight="32" tilecount="10" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="7">
  <properties>
   <property name="blocked" value=""/>
  </properties>
  <image width="32" height="32" source="corner.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="blocked" value=""/>
  </properties>
  <image width="32" height="32" source="cross piece.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="blocked" value=""/>
  </properties>
  <image width="32" height="32" source="end.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="blocked" value=""/>
  </properties>
  <image width="32" height="32" source="Straight.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="blocked" value=""/>
  </properties>
  <image width="32" height="32" source="t-piece.png"/>
 </tile>
 <tile id="12">
  <image width="32" height="32" source="walk.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="blocked" value=""/>
  </properties>
  <image width="32" height="32" source="circle.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="powerup" value="true"/>
  </properties>
  <image width="32" height="32" source="powerUp.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="spawn" value=""/>
  </properties>
  <image width="32" height="32" source="spawn.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="ghostDoor" value=""/>
  </properties>
  <image width="32" height="32" source="ghostDoor.png"/>
 </tile>
</tileset>
